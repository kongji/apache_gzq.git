package com.apacheGzq.pojo;

import org.apache.commons.lang.time.DateUtils;

import com.apacheGzq.constant.Constants;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaozhiqiang .
 */
public class HttpResponse {
    private String responseStatusLine = "HTTP/1.0 200 OK";
    private String server = "MY HTTP SERVER/0.1";
    private String dateStr = DateFormat.getDateInstance().format(new Date());
    private String connection = "Keep-Alive";
    private long contentLength = 0;
    private String contentType = "text/html";
    private String expires = DateFormat.getDateInstance().format(DateUtils.addHours(new Date(), 1));
    private byte[] responseBody = null;



    public byte[] getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(byte[] responseBody) {
        this.responseBody = responseBody;
    }

    public String getResponseStatusLine() {
        return responseStatusLine;
    }

    public void setResponseStatusLine(String responseStatusLine) {
        this.responseStatusLine = responseStatusLine;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public long getContentLength() {
        if(this.getResponseBody() != null) {
            contentLength = this.getResponseBody().length;
        }
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String toString() {
        return this.getResponseStatusLine() + Constants.CRLF
                + "Server: "+this.getServer() + Constants.CRLF
                + "Date: "+this.getDateStr() + Constants.CRLF
                + "Connection: "+this.getConnection() + Constants.CRLF
                + "Content-Length: "+this.getContentLength() + Constants.CRLF
                + "Content-Type: "+this.getContentType() + Constants.CRLF
                + "Expires: " + this.getExpires() + Constants.CRLF
                + Constants.CRLF
                + Constants.CRLF;
    }

}

