package com.apacheGzq.main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.apacheGzq.config.SysConfig;
import com.apacheGzq.thread.ListenTask;

import java.io.File;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.concurrent.*;

/**
 * Created by gaozhiqiang .
 */
public class MyServer {

    static {
        PropertyConfigurator.configureAndWatch(System.getProperty("user.dir")+ File.separator+"config"+File.separator+"log4j.properties");
    }

    private static final Logger log = Logger.getLogger(MyServer.class);

    public static volatile int runStatus = MyServer.RUNNING;
    public static final int RUNNING = 1;
    public static final int STOP = 0;


    public static void main(String[] args) {

        if(!SysConfig.init("")) {
            log.error("初始化配置文件错误，系统退出.");
            System.exit(-1);
        } else {
            log.info("初始化配置文件完成，开始启动服务...");
        }

        ExecutorService threadPool = Executors.newCachedThreadPool();

        Future future = threadPool.submit(new ListenTask(threadPool));

        try {
            if(future.get() == null) {
                log.info("监听客户端请求结束.");
            };
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if(runStatus == MyServer.STOP) {
            threadPool.shutdownNow();
        }
    }

}


