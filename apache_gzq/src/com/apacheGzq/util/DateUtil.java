package com.apacheGzq.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by gaozhiqiang .
 */
public class DateUtil {

    /**
     * 获取GMT格式时间串
     * @return "EEE, dd MMM yyyy HH:mm:ss z"
     */
    public static String GetGMTStr() {
        DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(new Date());
    }

    /**
     * 根据传入时间获取GMT格式时间串
     * @param date
     * @return "EEE, dd MMM yyyy HH:mm:ss z"
     */
    public static String GetGMTStr(Date date) {
        DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        if(date == null) {
            date = new Date();
        }
        return format.format(date);
    }

    /**
     * 获取8位日期
     * @return "yyyyMMdd"
     */
    public static String GetDateStr() {
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        return format.format(new Date());
    }

    /**
     * 获取6位时间
     * @return "HHmmss"
     */
    public static String GetTimeStr() {
        DateFormat format = new SimpleDateFormat("HHmmss");
        return format.format(new Date());
    }

    /**
     * 获取14位时间串
     * @return "yyyyMMddHHmmss"
     */
    public static String GetDateTimeStr() {
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return format.format(new Date());
    }

}
