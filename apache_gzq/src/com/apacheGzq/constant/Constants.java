package com.apacheGzq.constant;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaozhiqiang .
 */
public class Constants {

    /**
     * 服务器IP，默认使用本机地址：127.0.0.1
     */
    public static String SERVERIP = "127.0.0.1";

    /**
     * 服务器端口，默认使用80端口
     */
    public static int SERVERPORT = 8080;

    /**
     * 服务器可接受的最大连接，超过连接数拒绝
     */
    public static int BACKLOG = 1;

    /**
     * 缓冲区大小
     */
    public static int BUFFERSIZE = 1024;

    /**
     * 缓冲区编码格式
     */
    public static String CHARSETNAME = "UTF-8";

    /**
     * 存储访问路径与实际代码所在位置对应关系
     */
    public static Map<String, String> CONTENTPATH_MAP = new HashMap<String, String>();

    public static String CRLF = "\r\n";
    public static String SCHEME = "http";
}

