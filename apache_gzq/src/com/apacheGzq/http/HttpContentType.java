package com.apacheGzq.http;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaozhiqiang .
 */
public class HttpContentType {
    public static Map<String, String> contentTypeMap = new HashMap<String, String>();

    static {
        contentTypeMap.put(".*", "application/octet-stream");
        contentTypeMap.put(".bmp", "application/x-bmp");
        contentTypeMap.put(".doc", "application/msword");
        contentTypeMap.put(".gif", "image/gif");
        contentTypeMap.put(".htm", "text/html; charset=utf-8");
        contentTypeMap.put(".html", "text/html; charset=utf-8");
        contentTypeMap.put(".java", "java/*");
        contentTypeMap.put(".jpe", "image/jpeg");
        contentTypeMap.put(".jpeg", "image/jpeg");
        contentTypeMap.put(".jpg", "application/x-jpg");
        contentTypeMap.put(".jsp", "text/html; charset=utf-8");
        contentTypeMap.put(".xhtml", "text/html; charset=utf-8");
        contentTypeMap.put(".mid", "audio/mid");
        contentTypeMap.put(".mpeg", "video/mpg");
        contentTypeMap.put(".xml", "text/xml; charset=utf-8");
        contentTypeMap.put(".txt", "text/plain; charset=utf-8");
        contentTypeMap.put(".js", "application/x-javascript");
        contentTypeMap.put(".png", "image/png");
        contentTypeMap.put(".css", "text/css");
    }

}

