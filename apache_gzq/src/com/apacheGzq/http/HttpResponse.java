package com.apacheGzq.http;

/**
 * Created by gaozhiqiang .
 */
public interface HttpResponse {
    /**
     * 获取HTTP协议版本号
     * @return "HTTP/1.1"
     */
    public String protocolVersion();

    /**
     * 获取状态码
     * @return "200"
     */
    public String statusCode();

    /**
     * 状态码描述
     * @return "OK"
     */
    public String reasonPhrase();

}
