package com.apacheGzq.http;


import java.util.Enumeration;
import java.util.Map;

/**
 * Created by gaozhiqiang .
 */
public interface HttpRequest {

    public static final String ACCEPT = "Accept";
    public static final String ACCEPT_CHARSET = "Accept-Charset";
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String ACCEPT_RANGES = "Accept-Ranges";
    public static final String AUTHORIZATION = "Authorization";
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String CONNECTION = "Connection";
    public static final String COOKIE = "Cookie";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DATE = "Date";
    public static final String EXPECT = "Expect";
    public static final String FROM = "From";
    public static final String HOST = "Host";
    public static final String IF_MATCH = "If-Match";
    public static final String IF_MODIFIED_SINCE = "If-Modified-Since";
    public static final String IF_NONE_MATCH = "If-None-Match";
    public static final String IF_RANGE = "If-Range";
    public static final String IF_UNMODIFIED_SINCE = "If-Unmodified-Since";
    public static final String MAX_FORWARDS = "Max-Forwards";
    public static final String PRAGMA = "Pragma";
    public static final String PROXY_AUTHORIZATION = "Proxy-Authorization";
    public static final String RANGE = "Range";
    public static final String REFERER = "Referer";
    public static final String TE = "TE";
    public static final String UPGRADE = "Upgrade";
    public static final String USER_AGENT = "User-Agent";
    public static final String VIA = "Via";
    public static final String WARNING = "Warning";


    /**
     * 获取请求方法 (OPTIONS HEAD GET POST PUT DELETE TRACE CONNECT PATCH)
     * @return "GET"
     */
    public String getMethod();

    /**
     * 获取请求资源
     * @return "/index.html"
     */
    public String getRequestUri();

    /**
     * 获取请求路径
     * @return "http://localhost/index.html"
     */
    public String getRequestUrl();

    /**
     * 获取访问协议
     * @return "http"
     */
    public String getScheme();

    /**
     * 返回服务器主机名
     * @return localhost
     */
    public String getServerName();

    /**
     * 返回服务器端口号
     * @return 8080
     */
    public int getServerPort();

    /**
     * 返回服务器主机名含端口号
     * @return localhost:8080
     */
    public String getHost();

    /**
     * 设置请求IP
     */
    public String setRemoteAddr(String remoteAddr);
    /**
     * 返回请求IP
     * @return
     */
    public String getRemoteAddr();

    /**
     * 返回查询字符串
     * @return
     */
    public String getQueryString();

    /**
     * 查询请求参数
     * @param name 参数名称
     * @return 参数值
     */
    public String getParameter(String name);

    /**
     * 获取请求头参数
     * @param name 参数名称
     * @return 参数值
     */
    public String getHeader(String name);

    /**
     * 获取request中保存的对象
     * @param name 参数名
     * @return 参数值
     */
    public Object getAttribute(String name);

    /**
     * 将对象保存到request中
     * @param name      参数名称
     * @param object    参数值
     */
    public void setAttribute(String name, Object object);

    /**
     * 设置字符编码
     * @param encodingName
     * @return
     */
    public String setCharacterEncoding(String encodingName);

    /**
     * 获取字符编码
     * @return
     */
    public String getCharacterEncoding();

    /**
     * 获取HTTP协议版本号
     * @return "HTTP/1.1"
     */
    public String getProtocolVersion();

    /**
     * 判断请求方法服务器是否支持, false时状态码应为405
     * @return true/false
     */
    public boolean isAllowMethod();

    /**
     * 判断请求方法服务器是否实现，false时状态码应为501
     * @return true/false
     */
    public boolean isImplemented();

    /**
     * 建议request请求报文
     * @param request
     */
    public void parserRequest(String request);

}

